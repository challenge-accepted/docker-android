FROM rubdos/android-java8

RUN apt-get update && apt-get install -y libswt-gtk-3-jni libswt-gtk-3-java vim net-tools gradle
RUN update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

WORKDIR /root

ADD eclipse.tar.gz /root

CMD ["/bin/bash"]

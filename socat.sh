#!/bin/bash

PROCESS=socat

ps cax | grep $PROCESS > /dev/null
if [ $? -eq 0 ]; then
  echo "$PROCESS is already running."
else
  socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\" &
fi

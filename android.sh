#!/bin/bash

OPTION=$1
EXTRA=${@:2}
LOCALIP=`ifconfig | awk '/inet/{print substr($2, 0)}' | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | grep -voE "\b(127\.)([0-9]{1,3}\.){2}[0-9]{1,3}\b"`
NAME=android
LINKS=""
COMMAND="/bin/bash"
IMAGE="arlindosilvaneto/android"
PORTS=""
VOLUMES="-v `pwd`/projects:/root/projects -v `pwd`/m2:/root/.m2  -v `pwd`/workspace:/root/workspace -v `pwd`/eclipse-settings:/root/.eclipse"
ENV="-e DISPLAY=$LOCALIP:0" # YOUR IP, BUT NOT LOCALHOST

if [ -z $OPTION ]; then
	OPTION=run
fi

if [ $OPTION = "run" ]; then
	docker run --rm -it $PORTS $ENV --name $NAME $VOLUMES $LINKS $IMAGE $COMMAND $EXTRA
fi

if [ $OPTION = "rm" ]; then
	docker rm -f $NAME
fi

if [ $OPTION = "stop" ]; then
	docker stop $NAME
fi

if [ $OPTION = "start" ]; then
	docker start $NAME
fi

if [ $OPTION = "attach" ]; then
	docker attach $NAME
fi

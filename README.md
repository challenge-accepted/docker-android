# Android DOCKER

## on MAC

```
open -a XQuartz
./socat.sh
./eclipse.sh
```

## inside Docker container

```
eclipse/eclipse &
```

## on Windows

```
Xming -ac -multiwindow -clipboard
docker run -it --rm --name eclipse -e DISPLAY=<your IP>:0.0 -v "%cd%"\projects:/home/user/projects -v "%cd%"\m2:/home/user/.m2 -v "%cd%"\workspace:/home/user/workspace -v "%cd%"\eclipse-settings:/home/user/.eclipse arlindosilvaneto/eclipse
```

## inside Docker container

```
eclipse/eclipse &
```
